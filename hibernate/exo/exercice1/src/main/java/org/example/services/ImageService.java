package org.example.services;

import org.example.entities.Image;
import org.example.interfaces.IDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.Date;
import java.util.List;

public class ImageService  implements IDAO<Image> {

    private StandardServiceRegistry registry;
    private SessionFactory sessionFactory;

    private Session session;

    public ImageService(){
        registry=new StandardServiceRegistryBuilder().configure().build();
        sessionFactory=new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }


    @Override
    public boolean create(Image o) {
        Session session =sessionFactory.openSession();
        session.beginTransaction();
        session.save(o);
        o.getProduit().ajouterImage(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Image o) {
        return false;
    }

    @Override
    public boolean delete(Image o) {
        return false;
    }

    @Override
    public Image findById(int id) {
        return null;
    }

    @Override
    public List<Image> findAll() {
        return null;
    }

    @Override
    public List<Image> filterByPrice(double min) throws Exception {



        return null;
    }

    @Override
    public List<Image> filterByDate(Date min, Date max) throws Exception {
        return null;
    }

    @Override
    public List<Image> filterByStock(int max) throws Exception {
        return null;
    }

    @Override
    public Double sumStockByname(String marque) throws Exception {
        return null;
    }


}
