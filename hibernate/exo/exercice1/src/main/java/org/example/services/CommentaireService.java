package org.example.services;

import org.example.entities.Commentaire;
import org.example.interfaces.IDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.util.Date;
import java.util.List;

public class CommentaireService implements IDAO<Commentaire> {
    private StandardServiceRegistry registry;
    private SessionFactory sessionFactory;

    private Session session;

    public CommentaireService(){
        registry=new StandardServiceRegistryBuilder().configure().build();
        sessionFactory=new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }


    @Override
    public boolean create(Commentaire o) {
        Session session =sessionFactory.openSession();
        session.beginTransaction();
        session.save(o);
        o.getProduit2().ajoutCommentaire(o);
        session.getTransaction().commit();

        return true;
    }

    @Override
    public boolean update(Commentaire o) {
        return false;
    }

    @Override
    public boolean delete(Commentaire o) {
        return false;
    }

    @Override
    public Commentaire findById(int id) {
        return null;
    }

    @Override
    public List<Commentaire> findAll() {
        return null;
    }

    @Override
    public List<Commentaire> filterByPrice(double min) throws Exception {
        return null;
    }

    @Override
    public List<Commentaire> filterByDate(Date min, Date max) throws Exception {
        return null;
    }

    @Override
    public List<Commentaire> filterByStock(int max) throws Exception {
        return null;
    }

    @Override
    public Double sumStockByname(String marque) throws Exception {
        return null;
    }


}
