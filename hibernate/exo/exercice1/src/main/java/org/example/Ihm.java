package org.example;

import org.example.entities.Produit;
import org.example.services.ProduitService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ihm {

    private ProduitService produitService;
    private Scanner scanner;

    public Ihm(){

        produitService = new ProduitService();
        scanner = new Scanner(System.in);

    }

    public void start(){
        String choise;
        do{
            menu();
            choise = scanner.nextLine();
            switch ( choise){
                case "1":
                    valeurParMarque();
                    break;
                case "2":
                    moyenne();
                    break;
                case "3":
                    produitsMarque();
                    break;
                case "4":
                    deleteParMarque();
//                    deleteByMarque();
                    break;
            }
        }while (!choise.equals("0"));
    }

    private  void menu(){
        System.out.println("#################Menu##################");
        System.out.println("1 -- Afficher la valeur du stock par marque");
        System.out.println("2 -- prix moyen des  produits");
        System.out.println("3 -- produits de plusieurs marques");
        System.out.println("4 -- supprimer produits  par marque");
    }

    private  void valeurParMarque(){

        System.out.println("Merci de saisir la marque : ");
        String marque=scanner.nextLine();
        try{
            System.out.println("La valeur du stock est de : "+produitService.valeurStockParMarque(marque)+" euros");
        }
        catch (Exception ex){
            System.out.println("la valeur est de 0");
        }
    }

    private void moyenne(){
        try{
            System.out.println("Le prix moyen est de : "+produitService.moyenne()+" euros");
        }
        catch (Exception ex){
            System.out.println("Erreur de calcule moyen");
        }
    }

    private void produitsMarque(){

        List<String> marques = new ArrayList<>();
        marques.add("APPLE");
        marques.add("samsung");
        List<Produit> produits = null;
        try {
            produits = produitService.filterByMarques(marques);
            for (Produit pr : produits){
                System.out.println(pr.getId());
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
    private  void deleteParMarque(){
        System.out.println("Merci de saisir la marque : ");
        String  marque = scanner.nextLine();
        try{
            produitService.deleteByMarque(marque);
            System.out.println("Suppression ok");
        }
        catch (Exception ex){
            System.out.println("Erreur de suppression");
        }

    }


}
