package org.example;

import org.example.entities.Commentaire;
import org.example.entities.Image;
import org.example.entities.Produit;
import org.example.services.CommentaireService;
import org.example.services.ImageService;
import org.example.services.ProduitService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Exercice5 {

    private ProduitService produitService;

    private CommentaireService commentaireService;

    private ImageService imageService;

    private Scanner scanner;

    public Exercice5(){

        produitService = new ProduitService();
        commentaireService = new CommentaireService();
        imageService = new ImageService();
        scanner = new Scanner(System.in);
    }

    public void start() throws Exception {

        int choix;
        do {
            menu();
             choix = scanner.nextInt();

            switch (choix) {
                case 1:
                    ajoutImage();
                    break;
                case 2:
                    ajoutCommentaire();
                    break;
                case 3:
                   afficherProduit();
                  //  afficherProduitsAvecNote();

                    break;

            }
        }while (choix != 0);


    }

    public void menu() {

        System.out.println("-----------MENU-----------");
        System.out.println("1 -- Ajouter une image");
        System.out.println("2 -- Ajouter un commentaire");
        System.out.println("3 -- Afficher les produit avec une note supérieur ou égale à :");
        System.out.println("0 -- Quitter");
    }

    public void ajoutImage() {
        System.out.println("Entrer une Url");
        scanner.nextLine();
        String url = scanner.nextLine();
        System.out.println("Donner l'Id du poduit ou l'image doit être ajouter l'image");
        int idP = scanner.nextInt();

        Produit produit = produitService.findById(idP);

        if(produit == null)
            System.out.println("il n'y a pas de produit à cette Id");
        else
            imageService.create(new Image(url, produit));

    }

    public void ajoutCommentaire() throws ParseException {

        System.out.println("Entrer le contenue de votre commentaire");
        scanner.nextLine();
        String contenue = scanner.nextLine();
        System.out.println("Entrer la date du commentaire (dd/MM/yyyy :");
        String dates = scanner.nextLine();
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dates);
        System.out.println("Entrer une note entre 0 et 10:");
        int note= scanner.nextInt();
        System.out.println("Entrer l'Id du produit concerné");
        int idP = scanner.nextInt();

        Produit produit = produitService.findById(idP);

        if(produit == null)
            System.out.println("il n'y a pas de produit à cette Id");

        else
            commentaireService.create(new Commentaire(contenue,date,note,produit));


    }

    public void afficherProduit() {

        System.out.println("Rentrer la valeur de la note moyenne minimum");
        int min = scanner.nextInt();
        scanner.nextLine();

        List<Produit> produits= null;
        try {
            produits = produitService.filerByNote(min);
            for (Produit pr : produits) {
                System.out.println(pr.getId());
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

    }

    private void afficherProduitsAvecNote(){
        System.out.println("Merci de saisir la note min");
        int note = scanner.nextInt();
        scanner.nextLine();
        List<Produit> produits1= null;
        try {
            produits1 = produitService.getProduitsParNoteMin(note);
            for(Produit pr : produits1) {
                System.out.println(pr.getId()+" "+pr.getMarque());
            }
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


}
