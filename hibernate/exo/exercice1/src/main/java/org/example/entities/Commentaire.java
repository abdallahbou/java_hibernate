package org.example.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Commentaire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String contenue;

    private Date date;

    private int note;

    public Commentaire() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContenue() {
        return contenue;
    }

    public void setContenue(String contenue) {
        this.contenue = contenue;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "produit_id")
    private Produit produit2;

    public Produit getProduit2() {
        return produit2;
    }

    public void setProduit2(Produit produit2) {
        this.produit2 = produit2;
    }

    public Commentaire(String contenue, Date date, int note, Produit produit2) {
        this.contenue = contenue;
        this.date = date;
        this.note = note;
        this.produit2 = produit2;
    }

    @Override
    public String toString() {
        return "Commentaire{" +
                "id=" + id +
                ", contenue='" + contenue + '\'' +
                ", date=" + date +
                ", note=" + note +
                ", produit2=" + produit2 +
                '}';
    }
}
