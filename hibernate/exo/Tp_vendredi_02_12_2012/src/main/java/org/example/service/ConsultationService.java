package org.example.service;

import org.example.entity.FicheConsultation;
import org.example.entity.OperationAnalyse;
import org.example.entity.Prescription;
import org.example.interfaces.IDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class ConsultationService implements IDAO<FicheConsultation> {


    private StandardServiceRegistry registre;

    private SessionFactory sessionFactory;

    private Session session;

    public ConsultationService(){
        registre = new StandardServiceRegistryBuilder().configure().build();
        sessionFactory = new MetadataSources(registre).buildMetadata().buildSessionFactory();
        session = sessionFactory.openSession();
    }


    @Override
    public boolean create(FicheConsultation o) {
        session.beginTransaction();
        session.save(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public FicheConsultation findById(int id) {
        FicheConsultation ficheConsultation = null;
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        ficheConsultation=(FicheConsultation) session.get(FicheConsultation.class,id);
        session.getTransaction().commit();

        return ficheConsultation;
    }

    public boolean newPrescription(Prescription prescription, int id){
        boolean result= false;
        FicheConsultation ficheConsultation = this.findById(id);
        session.getTransaction().begin();
        if (ficheConsultation != null){
            prescription.setFicheConsultation(ficheConsultation);
            session.save(prescription);
        }
        session.getTransaction().commit();
        return result;

    }

    public boolean newAnalyse(OperationAnalyse operationAnalyse, int id){
        boolean result = false;
        FicheConsultation ficheConsultation=this.findById(id);
        session.getTransaction().begin();
        if (ficheConsultation != null) {
            operationAnalyse.setFicheConsultation(ficheConsultation);
            session.save(operationAnalyse);
        }
        session.getTransaction().commit();
        return result;
    }
}
