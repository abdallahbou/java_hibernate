package org.example.service;

import org.example.entity.DossierMedical;
import org.example.entity.FicheDePayement;
import org.example.entity.FicheDeSoin;
import org.example.entity.Patient;
import org.example.interfaces.IDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

public class DossierMedicalService implements IDAO<DossierMedical> {


    private StandardServiceRegistry registre;

    private SessionFactory sessionFactory;

    private Session session;

    public DossierMedicalService(){
        registre = new StandardServiceRegistryBuilder().configure().build();
        sessionFactory = new MetadataSources(registre).buildMetadata().buildSessionFactory();
        session = sessionFactory.openSession();
    }

    @Override
    public boolean create(DossierMedical o) {
        session.beginTransaction();
        session.save(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public DossierMedical findById(int id) {
        DossierMedical dossierMedical = null;
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        dossierMedical = (DossierMedical) session.get(DossierMedical.class, id);
        session.getTransaction().commit();
        return dossierMedical;
    }


    public boolean newPatient(Patient patient,int id){
        boolean result = false;
        DossierMedical dossierMedical = this.findById(id);
        session.getTransaction().begin();
        if (dossierMedical != null){
            patient.setDossierMedical(dossierMedical);
            session.save(patient);
        }

        session.getTransaction().commit();
        return result;
    }

    public boolean newSoin(FicheDeSoin ficheDeSoin, int id){
        boolean result = false;
        DossierMedical dossierMedical = this.findById(id);
        session.getTransaction().begin();
        if (dossierMedical != null){
            ficheDeSoin.setDossierMedical(dossierMedical);
            session.save(ficheDeSoin);
        }

        session.getTransaction().commit();
        return result;
    }

    public FicheDeSoin addSoinId(int id){
        FicheDeSoin ficheDeSoin = null;
        Session session = sessionFactory.openSession();
        ficheDeSoin = (FicheDeSoin) session.get(FicheDeSoin.class,id);
        session.getTransaction().commit();
        return  ficheDeSoin;
    }


    public boolean newPayement(FicheDePayement ficheDePayement, int id){

        boolean result = false;
        FicheDeSoin ficheDeSoin = this.addSoinId(id);
        session.getTransaction().begin();
        if(ficheDeSoin != null){
            ficheDePayement.setFicheDeSoins((List<FicheDeSoin>) ficheDeSoin);
            session.save(ficheDePayement);
        }
        session.getTransaction().commit();
        return result;
    }


}
