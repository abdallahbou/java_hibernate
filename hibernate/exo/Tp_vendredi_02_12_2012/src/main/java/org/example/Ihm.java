package org.example;

import org.example.entity.DossierMedical;
import org.example.entity.Patient;
import org.example.service.DossierMedicalService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Ihm {

    private DossierMedicalService dossierM;

    private Scanner scanner;

    public Ihm(){
        dossierM = new DossierMedicalService();

        scanner= new Scanner(System.in);

    }


    public void start() throws ParseException {

        int choix;
        do{
            menu();
            choix = scanner.nextInt();
            switch (choix){
                case 1: // creation d'un dossier medical
                    createDossier();
                    break;
                case 2:
                    break;
                case 3:
                    break;

            }
        }while (!(choix == 0));

    }


    public void menu(){
        System.out.println("------Bienvenue au menu de l'hôpital---------");
        System.out.println("1/ Créer un nouveau dossier médical ");
        System.out.println("2/ Afficher le dossier d'un patient  ");
        System.out.println("3/ Rechercher une consultation par date et patient");
        System.out.println("0/ Quitter le menu");
        System.out.println();
        System.out.println("Sélectionner où vous voulez aller");

    }

    public void createPatient() throws ParseException {
        System.out.println("------Creation d'un nouveau patient lier au dossier -------");
        System.out.println("saisire l'id du dossier");
        int id = scanner.nextInt();
        System.out.println("Entrer le nSS :");
        String nss = scanner.next();
        System.out.println("Prenom :");
        String prenom= scanner.next();
        System.out.println("Nom :");
        String nom = scanner.next();
        System.out.println("Date de naissance (dd/MM/yyyy) :");
        String date = scanner.next();
        Date date1= new SimpleDateFormat("dd/MM/yyyy").parse(date);
        System.out.println("Sexe h ou f");
        Character sexe = scanner.next().charAt(0);
        System.out.println("Adresse : ");
        scanner.nextLine();
        String adresse=scanner.nextLine();
        System.out.println("Numéro de télépone");
        int num = scanner.nextInt();

        Patient patient = new Patient(nss,prenom,nom,date1,sexe,adresse,num);
        if(dossierM.newPatient(patient,id)){
            System.out.println("patient ajouter dans le dossier" );

        }else{
            System.out.println("erreur: il n'y a pas eu d'ajout de patient");
        }


        }




    public void createDossier() throws ParseException {

        System.out.println("------Creation d'un nouveau dossier -------");
        System.out.println("Entrer le numéro du dossier");
        int numero= scanner.nextInt();
        System.out.println("Entrer le code access du patient");
        String code = scanner.next();

        DossierMedical dossierMedical = new DossierMedical(numero,code);
        if (dossierM.create(dossierMedical)) {
            System.out.println("Dossier créer");
            System.out.println();
            createPatient();
        }else{
            System.out.println("erreur d'ajout de dossier");
        }


    }

    public void menuConsultation(){

        System.out.println("------Menu consultation -------");
        System.out.println("1/ Créer une fiche de consultation");
        System.out.println("");

    }


}
