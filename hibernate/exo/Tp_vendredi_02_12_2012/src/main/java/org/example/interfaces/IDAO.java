package org.example.interfaces;

import java.util.List;

public interface IDAO<T> {

    boolean create(T o);

    T findById(int id);



}
