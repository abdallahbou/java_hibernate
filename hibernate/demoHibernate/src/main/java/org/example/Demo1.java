package org.example;

import org.example.entities.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class Demo1 {


    public static void main(String[] args) {

//        Creation d'un registre pour charger la configuration à partir de notre fichier de configuration

        StandardServiceRegistry registre = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registre).buildMetadata().buildSessionFactory();

//        Création de la session

        Session session = sessionFactory.openSession();

//        Dés l'ouverture de la session, et en fonction de la propriété hibernate.hbm2ddl.auto
//        hibernate va agir sur la base de donnée


        //L'ajout d'une personne

        Personne p = new Personne();
        p.setNom("tristane");
        p.setPrenom("lucas");
        p.setAge(21);
        session.save(p);
        System.out.println("Id : " + p.getId());


        //Récupérer une personne
        //  c'est important d'être dans même transaction si on souhaite modifier ou supprimer

//        session.getTransaction().begin();
//        Personne p = session.load(Personne.class,Long.valueOf(3));
//        System.out.println(p.getNom());
//
//
//
//        //Modifie
//        p.setNom("titi");
//        session.update(p);
//
//        //Suppression
//
//        session.delete(p);
//
//
//
//        session.getTransaction().commit();
//        session.close();
//        sessionFactory.close();
    }

}
