package org.example;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.NativeQuery;

import java.lang.annotation.Native;
import java.util.List;

public class Demo5 {

    public static void main(String[]args){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();


        // Native SQL

        NativeQuery query = session.createSQLQuery("select * from Personne");
        List personnes = query.list(); // le resultat est une  liste de tableau objet non typé

        for (Object o :personnes) {
            Object[] p =(Object[]) o;
            System.out.println(p[0] + " "+p[2]+" "+p[3]);
        }

    }

}
