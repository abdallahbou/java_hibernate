package org.example;

import org.example.entities.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class Demo4 {
    public static void main(String[] args) {
        StandardServiceRegistry registre = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registre).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();

        //Utilisation des  fonctions d'aggrégation

        Query<Integer> queryMaxAge = session.createQuery("select max(age) from Personne");
        int maxAge = queryMaxAge.uniqueResult();

        System.out.println(maxAge);

        double ageMoyen = (double)session.createQuery("select avg(age) from Personne").uniqueResult();
        System.out.println("Age Moyen : "+ageMoyen);

        //Utilisation de la cluse IN

        List noms = new ArrayList<String >();
        noms.add("toto");
        noms.add("titi");

        Query<Personne> query = session.createQuery("from Personne where nom in :noms");
        query.setParameter("noms",noms);

        List<Personne> personnes =query.list();

        for(Personne p : personnes){
            System.out.println(p.getNom());  }

            //Utilisation de l'excuse update ou le excuse delete
            System.out.println("test");
            String update_query = "update Personne set nom=:nomP where id=2";
            Query query1 = session.createQuery(update_query);
            query1.setParameter("nomP","Sirina");
            session.getTransaction().begin();
            int success = query1.executeUpdate();// nombre de ligne effectué par la requete
            session.getTransaction().commit();
            System.out.println(success);

    }
}
