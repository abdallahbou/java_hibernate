package org.example.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "enseignant")
public class Enseignant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="code_ens")
    private int code;

    @Column(name="nom_ens", length = 30)
    private String nom;

    @Column(name="prenom_ens",length = 30)
    private String prenom;

    @ManyToMany(mappedBy = "enseignants")
    private Set<Matiere> matiereSet = new HashSet<>();

    public Enseignant() {
    }

    public Enseignant(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public Set<Matiere> getMatiereSet() {
        return matiereSet;
    }

    public void setMatiereSet(Set<Matiere> matiereSet) {
        this.matiereSet = matiereSet;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
}
