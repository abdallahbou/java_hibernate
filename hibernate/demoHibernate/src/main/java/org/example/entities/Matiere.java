package org.example.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "matiere")
public class Matiere {

    @Id
    @Column(name="code_mat",length = 6)
    private String code;


    @Column(name = "libelle_mat",length = 100)
    private String libelle;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @ManyToMany
    @JoinTable(name="enseigner",
            joinColumns = @JoinColumn(name="code_mat"),
            inverseJoinColumns=@JoinColumn(name="code_ens"))
    private Set<Enseignant> enseignants = new HashSet<>();

    public Set<Enseignant> getEnseignants() {
        return enseignants;
    }

    public void setEnseignants(Set<Enseignant> enseignants) {
        this.enseignants = enseignants;
    }

    public void ajouterEnseignant(Enseignant ens){this.enseignants.add(ens);}

    public Matiere() {
    }

    public Matiere(String code, String libelle) {
        this.code = code;
        this.libelle = libelle;
    }
}
