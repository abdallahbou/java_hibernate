package org.example;

import org.example.entities.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import org.hibernate.type.StringType;

import java.util.ArrayList;
import java.util.List;

public class Demo3 {

    public static void main(String[] args) {
        StandardServiceRegistry registre = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registre).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();

        //Récupération des element avec des parametres dans le filtre.
        String search = "t";

        //parametre avec des noms

       // Query<Personne> personneQuery = session.createQuery("from Personne nom like :nom");

        // dans le support on utilise setString et detInteger elles sont deprecated
//
//        personneQuery.setParameter("nom",search+"%", StringType.INSTANCE);
//        List<Personne> personnes = personneQuery.list();

        //parametre avec des positions
        // il faur=t indiquer le numéro de parametre en plus de ?

//        Query<Personne> personneQuery1= session.createQuery("from Personne where from nom like ?1");
//        personneQuery1.setParameter(1,search+"%",StringType.INSTANCE);
//        List<Personne>personnes1=personneQuery1.list();

        //utiliser des alias comme en sql

//        Query<Personne> personneQuery2 = session.createQuery("from Personne as p where p.nom like ?1");
//        personneQuery2.setParameter(1,search+"%", StringType.INSTANCE);
//        List<Personne> personnes2 = personneQuery2.list();

        //Utiliser un select comme en sql
        // Attention le resultat ne sera pas une arrayList d'objet de mêm type mais un arrayList de tableau d'objet

        Query<Personne> personneQuery3 = session.createQuery("SELECT nom, prenom from Personne as p where p.nom like ?1");

        personneQuery3.setParameter(1,search+"%", StringType.INSTANCE);
        List result = personneQuery3.list();
        List<Personne> personnes3 = new ArrayList<>();

        //Convertir le resultat en list d'objet personne
        for(Object o : result){
            Object[] res=((Object[]) o);
            Personne pr = new Personne();
            pr.setNom((String)res[0]);
            pr.setPrenom((String) res[1]);
            personnes3.add(pr);


            for (Personne p :personnes3){
                System.out.println(p.getNom());
                System.out.println(p.getPrenom());
            }
        }

    }
}
