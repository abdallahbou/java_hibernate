package org.example;

import org.example.entities.Enseignant;
import org.example.entities.Matiere;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class Demo7 {


    public static void main(String[] args){


        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction tx = session.getTransaction();
        tx.begin();
        Enseignant ens1 = new Enseignant("Youness","Boukouchi");
        session.save(ens1);
        Matiere mat1 = new Matiere("JEE","Java Entreprise Edition");
        mat1.ajouterEnseignant(ens1);
        Matiere mat2 = new Matiere("SAO","Architecture Orientée Service");
        mat2.ajouterEnseignant(ens1);
        session.save(mat1);
        session.save(mat2);
        tx.commit();
        session.refresh(ens1);
        System.out.println("size : "+ens1.getMatiereSet().size());
        session.close();


    }


}
