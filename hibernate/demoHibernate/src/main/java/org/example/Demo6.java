package org.example;

import org.example.entities.Entreprise;
import org.example.entities.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;


import java.util.List;

public class Demo6 {

    public static void main(String[] args){

        StandardServiceRegistry registre = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registre).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();

        // Many to One

//        Transaction transaction = session.getTransaction();
//        transaction.begin();


        // creation d'une entreprise

//        Entreprise entreprise = new Entreprise();
//        entreprise.setNom("M2I");
//
//        session.save(entreprise);
//
//        Personne personne = new Personne();
//
//        personne.setNom("Titi");
//        personne.setPrenom("Tata");
//        personne.setAge(20);
//        personne.setEntreprise(entreprise);
//
//        session.save(personne);
//
//        entreprise.ajouterPersonne(personne);
//
//        transaction.commit();
//
//        Query<Personne> personneQuery = session.createQuery("from Personne");
//        List<Personne> personneList= personneQuery.list();
//
//        for (Personne p : personneList){
//            Entreprise e = p.getEntreprise();
//            System.out.println(e.getNom());
//        }


        session.getTransaction().begin();
        Entreprise entreprise = session.get(Entreprise.class,1);
        Personne personne = new Personne();

        personne.setNom("Noel");
        personne.setPrenom("Pere");
        personne.setEntreprise(entreprise);
        session.save(personne);
        entreprise.ajouterPersonne(personne);
        session.update(entreprise);
        session.getTransaction().commit();

    }


}
